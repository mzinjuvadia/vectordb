from openai import OpenAI
from pgvector.psycopg2 import register_vector
from pgvector.psycopg2 import cast_vector

import psycopg2

import os

client = OpenAI(api_key=os.getenv('OPENAI_API_KEY'))

model="text-embedding-ada-002"

def get_embedding(text, model):
   text = text.replace("\n", " ")
   return client.embeddings.create(input = [text], model=model).data[0].embedding

conn = psycopg2.connect("dbname=mannanzinjuvadia user=postgres password=postgres host=localhost port=5432")
cur = conn.cursor()
register_vector(conn)

reviews = ""

question = "Are you open on a Saturday?"
embedding= str(get_embedding(question, model))
cur.execute('SELECT * FROM items ORDER BY embedding <=> %s LIMIT 10', (cast_vector(embedding, cur),))
# Change this to get the text complement of the vectorized portion of the table
KnearestVectors = cur.fetchall()
for item in KnearestVectors:
   reviews = reviews + item[1] + " "
   

query = f"""Use the below context and the below reviews to answer the following question. If an answer cannot be found reply with: I don't know the answer to that. Would you like me to put you in touch with our sales professsionals?

Context:
Store Name: Tony's Pizza Napoletana

Store Description: Bustling Italian eatery with varied pizza options from coal-fired to Roman-style, plus beer on tap.

Store Hours:
Sunday: 12:00PM to 10:30PM
Monday: 12:00Pm to 9:00PM
Tuesday: 12:00Pm to 9:30PM
Wednesday: 12:00PM to 10:00PM
Thursday: 12:00PM to 10:00PM
Friday: 12:00PM to 11:00PM
Saturday: 12:00PM to 11:00PM

Accessibility:
Wheelchair accessible entrance
Wheelchair accessible restroom
Wheelchair accessible seating
Wheelchair accessible parking lot

Service options:
Outdoor seating
No-contact delivery
Delivery
Takeout
Dine-in

Dining options:
Lunch
Dinner
Catering
Counter service
Dessert
Seating

Amenities:
Bar onsite
Gender-neutral restroom
Restroom

Atmosphere:
Casual
Cozy
Historic
Trendy

Payments:
Credit cards
Debit cards
NFC mobile payments
Credit cards

Children:
Good for kids
High chairs
Kids' menu

Parking:
Paid street parking
Difficult to find a space

Reviews: {reviews}

Question: {question}
"""

response = client.chat.completions.create(
  model="gpt-3.5-turbo-0125",
  response_format={ "type": "json_object" },
  messages=[
    {"role": "system", "content": "You are a helpful assistant designed to output JSON."},
    {"role": "user", "content": query}
  ]
)
print(response.choices[0].message.content)   

