from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import StaleElementReferenceException

from pgvector.psycopg2 import register_vector

from openai import OpenAI

import psycopg2

import numpy as np

import os

conn = psycopg2.connect("dbname=mannanzinjuvadia user=postgres password=postgres host=localhost port=5432")
cur = conn.cursor()
register_vector(conn)

client = OpenAI(api_key=os.getenv('OPENAI_API_KEY'))

model="text-embedding-ada-002"

def get_embedding(text, model):
   text = text.replace("\n", " ")
   print("1")
   return client.embeddings.create(input = [text], model=model).data[0].embedding

options = Options()
options.add_argument("--headless")
driver = webdriver.Chrome(options=options)
url= "https://www.google.com/maps/place/Tony's+Pizza+Napoletana/@37.8003401,-122.4115976,16z/data=!4m17!1m8!3m7!1s0x808580f112dc711d:0xbfb3fcde3487fb40!2sTony's+Pizza+Napoletana!8m2!3d37.8003359!4d-122.4090227!10e1!16s%2Fg%2F11b900mvkk!3m7!1s0x808580f112dc711d:0xbfb3fcde3487fb40!8m2!3d37.8003359!4d-122.4090227!9m1!1b1!16s%2Fg%2F11b900mvkk?entry=ttu"
driver.get(url)

wait = WebDriverWait(driver, 300)
wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "jftiEf")))

countReviews = 2000

totalReviews = int(driver.find_element(By.CLASS_NAME, 'jANrlb').find_element(By.CLASS_NAME, 'fontBodySmall').text.split(" ")[0].replace(",", ""))
    
def clickButton():
    wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "w8nwRe")))
    element = driver.find_elements(By.CLASS_NAME, 'w8nwRe')
    for button in element:
        button.click()

def infinite_scroll(driver):
    number_of_elements_found = 0
    if countReviews > 0:
        while True:
            els = driver.find_elements(By.CLASS_NAME, 'qCHGyb')
            if number_of_elements_found > countReviews:
                break

            try:
                driver.execute_script("arguments[0].scrollIntoView();", els[-1])
                clickButton() 
                number_of_elements_found = len(els)
                print(number_of_elements_found)

            except StaleElementReferenceException:
                pass
    else:
        print("Cannot have countReview = 0")        

infinite_scroll(driver)

allReviews = driver.find_elements(By.CLASS_NAME, 'wiI7pd')

for item in allReviews:
    cur.execute('INSERT INTO items (reviews, embedding) VALUES (%s, %s)', (item.text, get_embedding(item.text, model)))

conn.commit()